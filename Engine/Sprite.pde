class Sprite extends Component {
  PImage sprite;
  int spriteWidth;
  int spriteHeight;
  
  Sprite() {
    
  }
  
  Sprite(PImage sprite) {
    this.spriteWidth = sprite.width;
    this.spriteHeight = sprite.height;
    this.sprite = sprite;
  }
  
  Sprite(int spriteWidth, int spriteHeight, PImage sprite) {
    this.spriteWidth = spriteWidth;
    this.spriteHeight = spriteHeight;
    this.sprite = sprite;
  }
  
  Sprite(String spritePath) {
    sprite = loadImage(spritePath);
    this.spriteWidth = sprite.width;
    this.spriteHeight = sprite.height;
  }
  
  Sprite(int spriteWidth, int spriteHeight, String spritePath) {
    this.spriteWidth = spriteWidth;
    this.spriteHeight = spriteHeight;
    sprite = loadImage(spritePath);
  }
  
  @Override
  void update(double deltaTime) { }
}
