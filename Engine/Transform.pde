class Transform {
  PVector position;
  PVector rotation;
  PVector scale;
  
  Transform() {
    position = new PVector(0,0,0);
    rotation = new PVector(0,0,0);
    scale = new PVector(1,1,1);
  }
  
  Transform(PVector position, PVector rotation, PVector scale) {
    this.position = position;
    this.rotation = rotation;
    this.scale = scale;
  }
}
