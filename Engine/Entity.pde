class Entity {
  int id;
  Transform transform;

  Entity(int entityId) {
    id = entityId;
    transform = new Transform();
  }

  Entity(int entityId, Transform transform) {
    id = entityId;
    this.transform = transform;
  }
}
