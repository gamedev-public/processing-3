class ComponentSystem {
  // used for galaxy rendering
  float turn;
  
  void update(EntityManager manager) {
    updatePhysics(manager);
    renderSprites(manager);
    removeOutOfBoundsRigidbodies(manager);
  }

  private void updatePhysics(EntityManager manager) {
    ArrayList<Rigidbody> rigidbodies = manager.getAllComponentsOfType(Rigidbody.class);
    if (rigidbodies.size() > 0) {
      for (int index = 0; index < rigidbodies.size(); index++) {
        Rigidbody rigidbody = rigidbodies.get(index);
        PVector entityPosition = rigidbody.entity.transform.position;
        entityPosition.x += rigidbody.velocity.x;
        entityPosition.y += rigidbody.velocity.y;
      }
    }
  }

  private void renderSprites(EntityManager manager) {
    ArrayList<Sprite> sprites = manager.getAllComponentsOfType(Sprite.class);
    if (sprites.size() > 0) {
      for (int index = 0; index < sprites.size(); index++) {
        Sprite sprite = sprites.get(index);
        PVector entityPosition = sprite.entity.transform.position;
        image(sprite.sprite, entityPosition.x, entityPosition.y, sprite.spriteWidth, sprite.spriteHeight);
      }
    }
  }

  private void removeOutOfBoundsRigidbodies(EntityManager manager) {
    ArrayList<Rigidbody> rigidbodies = manager.getAllComponentsOfType(Rigidbody.class);
    if (rigidbodies.size() > 0) {
      ArrayList<Entity> delete = new ArrayList<Entity>();
      for (int index = 0; index < rigidbodies.size(); index++) {
        Rigidbody rb = rigidbodies.get(index);
        PVector position = rb.entity.transform.position;
        if (position.x < 0  || position.x > width || position.y < 0 || position.y > height) {
          delete.add(rb.entity);
        }
      }
      for (int index = 0; index < delete.size(); index++) {
        manager.DestroyEntity(delete.get(index));
      }
    }
  }
}
