class Rigidbody extends Component {
  PVector velocity;
  float gravity;
  float drag;
  float dragTolerance;

  Rigidbody() {
    velocity = new PVector(0, 0);
    gravity = 9.82;
    drag = 9.82;
    dragTolerance = 0.01;
  }

  Rigidbody(float gravity, float drag, float dragTolerance) {
    velocity = new PVector(0, 0);
    this.gravity = gravity;
    this.drag = drag;
    this.dragTolerance = dragTolerance;
  }

  @Override
    void update(double deltaTime) {
    if (velocity.x < 0) {
      velocity.x += drag * deltaTime;
    } else if (velocity.x > 0) {
      velocity.x -= drag * deltaTime;
    } else if (velocity.x > -dragTolerance && velocity.x < dragTolerance) {
      velocity.x = 0;
    }
    velocity.y += gravity * deltaTime;
  }
}
