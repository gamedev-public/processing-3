class EntityManager { //<>// //<>//
  private ArrayList<Entity> entities;
  private HashMap<Entity, ArrayList<Component>> components;

  EntityManager() {
    entities = new ArrayList<Entity>();
    components = new HashMap<Entity, ArrayList<Component>>();
  }

  public Entity addEntity() {
    Entity entity = new Entity(entities.size() + 1);
    entities.add(entity);
    return entity;
  }

  public Entity addEntity(Transform transform) {
    Entity entity = addEntity();
    entities.get(entity.id - 1).transform = transform;
    return entity;
  }

  public boolean DestroyEntity(Entity entity) {
    try {
      components.remove(entity);
      entities.remove(entity);
      return true;
    } 
    catch(Exception ex) {
      return false;
    }
  }

  public Entity getEntity(int entityId) {
    for (int index = 0; index < entities.size(); index++) {
      if (entities.get(index).id == entityId) {
        return entities.get(index);
      }
    }
    return null;
  }

  public int addComponent(Entity entity, Class<Component> componentType) {
    try {
      ArrayList<Component> comps = components.get(entity);
      int compId = comps.size() + 1;
      Component component = null;
      component = componentType.cast(component);
      component.entity = entity;
      comps.add(component);
      components.replace(entity, comps);
      return compId;
    }
    catch(Exception ex) {
      return -1;
    }
  }

  public int addComponent(Entity entity, Component component) {
    if (components.containsKey(entity) == false) {
      components.put(entity, new ArrayList<Component>());
    }
    try {
      ArrayList<Component> comps = components.get(entity);
      int compId = comps.size() + 1;
      component.id = compId;
      component.entity = entity;
      comps.add(component);
      components.replace(entity, comps);
      return compId;
    }
    catch(Exception ex) {
      return -1;
    }
  }

  public <T> T getComponent(Entity entity, Class<T> targetClass) {
    try {
      ArrayList<Component> comps = components.get(entity);
      for (int index = 0; index < comps.size(); index++) {
        try {
          return targetClass.cast(comps.get(index));
        } 
        catch(ClassCastException ex) {
          continue;
        }
      }
    }
    catch(Exception ex) {
      return null;
    }
    return null;
  }

  public ArrayList<Component> getComponents(Entity entity) {
    try {
      return components.get(entity);
    }
    catch(Exception ex) {
      return null;
    }
  }
  
  public <T> ArrayList<T> getAllComponentsOfType(Class<T> componentType) { //<>// //<>//
    ArrayList<T> result = new ArrayList<T>();
    for(int index = 0; index < entities.size(); index++) {
      T component = getComponent(entities.get(index), componentType);
      if(component != null) {
        result.add(component);
      }
    }
    return result;
  }
  
  public void removeComponent(Component component) {
    ArrayList<Component> comps = components.get(component.entity);
    for(int index = 0; index < comps.size(); index++) {
      if(comps.get(index).id == component.id) {
        comps.remove(index);
        break;
      }
    }
    components.replace(component.entity, comps);
  }

  public boolean updateComponent(Entity entity, Component comp) {
    try {
      ArrayList<Component> comps = components.get(entity);
      for (int index = 0; index < comps.size(); index++) {
        if (comps.get(index).id == comp.id) {
          comps.set(index, comp);
          break;
        }
      }
      components.replace(entity, comps);
      return true;
    } 
    catch(Exception ex) {
      return false;
    }
  }

  public void updateAllComponents(double deltaTime) {
    for (int entityIndex = 0; entityIndex < entities.size(); entityIndex++) {
      Entity entity = entities.get(entityIndex);
      ArrayList<Component> comps = components.get(entity);
      for (int compIndex = 0; compIndex < comps.size(); compIndex++) {
        Component comp = comps.get(compIndex);
        comp.update(deltaTime);
        comps.set(compIndex, comp);
      }
      components.replace(entity, comps);
    }
  }
}
