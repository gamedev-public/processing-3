float timeScale = 1.0;
double lastRecordedTime = 0.0;
double deltaTime = 0.0;
double unscaledDeltaTime = 0.0;
boolean showDebugTitleInfo = true;

EntityManager _entityManager;
ComponentSystem _componentSystem;

int uniformTileSize = 4;

void setup() {
  size(1024, 768, P2D);
  _entityManager = new EntityManager();
  _componentSystem = new ComponentSystem();
  lastRecordedTime = millis();
}

void draw() {
  deltaTime = ((millis() - lastRecordedTime) / 1000) * timeScale;
  unscaledDeltaTime = ((millis() - lastRecordedTime) / 1000);
  lastRecordedTime = millis();
  surface.setTitle(getTitleText());
  background(255);
  _entityManager.updateAllComponents(deltaTime*timeScale);
  _componentSystem.update(_entityManager);
  if (round(frameRate) > 15) {
    for(int counter = 0; counter < 10; counter++) {
      int rndX = (int)random(uniformTileSize, width-uniformTileSize);
      int rndY = (int)random(uniformTileSize, height-uniformTileSize);
      spawnCube(rndX, rndY);
    }
    
    if (mousePressed) {
      if (mouseButton == LEFT) {
        spawnCube(mouseX, mouseY);
      }
    }
  }
}

String getTitleText() {
  StringBuilder sb = new StringBuilder();
  sb.append(String.format("Entity Framework (FPS: %s)|", round(frameRate)));
  if (showDebugTitleInfo == true) {
    sb.append(String.format("Entities: %s|", _entityManager.entities.size()));
    sb.append(String.format("Rigidbodies: %s|", _entityManager.getAllComponentsOfType(Rigidbody.class).size()));
    sb.append(String.format("Sprites: %s|", _entityManager.getAllComponentsOfType(Sprite.class).size()));
  }
  return sb.toString();
} //<>//

void spawnCube(int x, int y) {
  Entity entity = _entityManager.addEntity();
  entity.transform.position = new PVector(x, y);
  Rigidbody rbody = new Rigidbody();
  Sprite sprite = new Sprite(uniformTileSize, uniformTileSize, "box.jpg");
  _entityManager.addComponent(entity, rbody);
  _entityManager.addComponent(entity, sprite);
}
