abstract class Component {
  Entity entity;
  int id;
  boolean isEnabled;
  
  Component() {
    isEnabled = true;
  }
  
  Component(Entity entity, int id) {
    this.entity = entity;
    this.id = id;
    isEnabled = true;
  }
  
  abstract void update(double deltaTime);
}
