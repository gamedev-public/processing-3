# How to use this Repository
This is an MIT licensed repository of Processin 3 projects. Every project should be self-contained and can be downloaded and run as-is.

# Suggestions and Bugs
Please submit issues on this repo if you find bugs or have suggestions. You can also add suggetions on my ko-fi page.

# Support
[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/M4M8674B7)

# Gallery
[<img height='256' src="https://i.imgur.com/4Q1nR3B.png">]() [<img height='256' src="https://i.imgur.com/8Fr1Os5.png">]() [<img height='256' src="https://i.imgur.com/FefUXgZ.png">]()
