// Tools
// Below are all the tools constants we need.
int toolsOffsetY = 192;                          // The vertical offset from the top of the screen.
int toolIndex = 0;                               // Which tool we are using right now.
PImage[] icons;                                  // Icons used for the tools
int penToolIndex = 0;                            // Pen tool index.
int lineToolIndex = 1;                           // Line tool index.
int colorPickerToolIndex = 2;                    // Color picker index.
int circleToolIndex = 3;                         // Circle tool index.
// Colors
// Below are all the color constants we need.
private color primaryPickedColor;                // The current primary color (left click/foreground color)
private color secondaryPickedColor;              // The current secondary color (right click/background color)
private Palette palette;                         // The color Palette object with all our colors.
private color[][] colorGrid = new color[4][4];   // The grid of color squares represented as a 2D array.
private int colorTileSize = 32;                  // The size of the cells used to show the colors.
private int colorGridOffsetY = 332;              // The vertical offset from the top of the screen.
// Grid
// Below are all the grid constants we need
private color[][] grid = new color[32][32];      // The grid that can be painted on represented as a 2D array.
private int tileSize = 16;                       // The size of each of the tiles that can be drawn o
private int gridOffset = 192;                    // The grids offset from both the top and left sides of the screen.
// General
// Below are the more general constants that might be needed
// in more than one place in the program.
private int leftOffsetX = 32;                    // The horizontal offset from the left side of the screen that all elements on the left side uses.
private int rightOffsetX = 148;                  // The horizontal offset from the right side of the screen that all elements on the right side uses.
private PFont font;                              // The application-wide font used.
private int systemTextSize = 16;                 // The application-wide font size.
private PImage saveIcon;                         // The Save icon.
private PImage saveQuitIcon;                     // The Save and Quit icon.
private boolean isQuittingAfterSave = false;     // Whether to quit the application after saving or not.

void setup() {
  size(732, 764);
  fill(126);
  font = createFont("Consolas Bold", 16);
  textFont(font);
  palette = new Palette();
  icons = new PImage[4];
  icons[0] = loadImage("pen.png");
  icons[1] = loadImage("line.png");
  icons[2] = loadImage("color_picker.png");
  icons[3] = loadImage("circle.png");
  saveIcon = loadImage("save.png");
  saveQuitIcon = loadImage("save_quit.png");
  // setup the grid that can be drawn up as a 2-dimensional array.
  // this means that you can consider this part of the screen a coordinate system.
  for (int y = 0; y < grid.length; y++) {
    for (int x = 0; x < grid[y].length; x++) {
      grid[y][x] = palette.getPaletteColor(palette.white);
    }
  }
  primaryPickedColor = palette.getPaletteColor(palette.black);
  secondaryPickedColor = palette.getPaletteColor(palette.white);
}

/*
* The paint tool is data driven. This means that tool, color and grid data is changed in-between draw() calls.
* This has the effect of giving the illusion of changing things immediately, even though the application is
* always one frame behind.
*/
void draw() {
  background(200);
  drawTitle();
  drawOptions();
  drawPaletteText();
  drawColorText();
  drawPreview();
  drawPointerText();
  drawToolGrid();
  drawColorPickerGrid();
  drawPaintingGrid();

  if (mousePressed) {
    if (isMouseInGrid() == true && toolIndex == penToolIndex) {
      PVector coords = getGridCoordinates();
      if (mouseButton == LEFT) {
        grid[(int)coords.x][(int)coords.y] = primaryPickedColor;
      } else if (mouseButton == RIGHT) {
        grid[(int)coords.x][(int)coords.y] = secondaryPickedColor;
      }
    } else if (isMouseInColorPicker() == true) {
      setPickedColor();
    } else if (isMouseInTools() == true) {
      setTool();
    }
  }
}

void mouseReleased() {
  if(isPressingSave()) {
    isQuittingAfterSave = false;
    selectOutput("Save Picture", "saveCallback");
  }
  if(isPressingSaveAndQuit()) {
    isQuittingAfterSave = true;
    selectOutput("Save Picture", "saveCallback");
  }
  if (isMouseInTools() == false) {
    if (toolIndex == colorPickerToolIndex) {
      color pickedColor = get(mouseX, mouseY);
      if (mouseButton == LEFT) {
        primaryPickedColor = pickedColor;
      } else if (mouseButton == RIGHT) {
        secondaryPickedColor = pickedColor;
      }
      toolIndex = penToolIndex;
    }
  }
}

/*
* Used by the selectOutput() call in mouseReleased().
*/
void saveCallback(File file) {
  if(file != null) {
    PImage image = createImage(grid.length, grid.length, RGB);
    int pixelIndex = 0;
    image.loadPixels();
    for(int y = 0; y < grid.length; y++) {
      for(int x = 0; x < grid[y].length; x++) {
        image.pixels[pixelIndex] = grid[y][x];
        pixelIndex++;
      }
    }
    image.updatePixels();
    image.save(file.getAbsolutePath());
    if(isQuittingAfterSave == true) {
      exit();
    }
  }
}

////////////////////
/// TEXT DRAWING ///
////////////////////
/*
* Used to draw the Title at the top of the screen.
*/
private void drawTitle() {
  stroke(55, 116, 182);
  fill(55, 116, 182);
  rect(0, 0, width, 32);
  textSize(systemTextSize);
  fill(255);
  // put the text in the middle of the width of the screen
  // and then eyeball the vertical value.
  text("DRAWING PROGRAM", width / 2 - 64, 20);
}

/*
* Used to draw the options called "Save" and "Save and Quit" at the top of the screen.
*/
private void drawOptions() {
  image(saveIcon, leftOffsetX, colorTileSize*2);
  image(saveQuitIcon, width-(colorTileSize*2), colorTileSize*2);
  textSize(systemTextSize);
  fill(palette.getPaletteColor(palette.black));
  text("SAVE", leftOffsetX, colorTileSize*3+24);
  text("SAVE AND QUIT", width-rightOffsetX, colorTileSize*3+24);
  
}

/*
* Draws the text for the palette on the left side of the screen, relative to the color tiles.
*/
private void drawPaletteText() {
  textSize(systemTextSize);
  fill(0);
  text("PALETTE", leftOffsetX, colorGridOffsetY - 8);
}

/*
* Draws the text, and squares, that shows the current picked Foreground and Background colors, relative to the color tiles. 
*/
private void drawColorText() {
  textSize(systemTextSize);
  fill(0);
  int foregrndY = colorGridOffsetY + (colorTileSize * (colorGrid.length + 1));
  int backgrndY = colorGridOffsetY + (colorTileSize * (colorGrid.length + 2));
  int squareSize = 16;
  text("FOREGRND", leftOffsetX, foregrndY);
  text("BACKGRND", leftOffsetX, backgrndY);
  noStroke();
  fill(primaryPickedColor);
  square(leftOffsetX * 4, foregrndY - (squareSize - 3), squareSize);
  fill(secondaryPickedColor);
  square(leftOffsetX * 4, backgrndY - (squareSize - 3), squareSize);
}

/*
* Draws the preview of the current drawing. 
*/
private void drawPreview() {
  int previewOffsetY = colorGridOffsetY + (colorTileSize * (colorGrid.length + 3) + tileSize + 3);
  textSize(systemTextSize);
  fill(palette.getPaletteColor(palette.black));
  text("PREVIEW", leftOffsetX, previewOffsetY - 8);
  strokeWeight(2);
  stroke(palette.getPaletteColor(palette.white));
  fill(palette.getPaletteColor(palette.white));
  PImage gridImage = createImage(grid.length, grid.length, RGB);
  int pixelCounter = 0;
  gridImage.loadPixels();
  for (int y = 0; y < grid.length; y++) {
    for (int x = 0; x < grid[y].length; x++) {
      gridImage.pixels[pixelCounter] = grid[y][x];
      pixelCounter++;
    }
  }
  gridImage.updatePixels();
  gridImage.resize(128, 128);
  image(gridImage, leftOffsetX, previewOffsetY);
}

/*
* Draws the pointer tracking text at the bottom of the screen relative to the big grid. 
*/
private void drawPointerText() {
  textSize(systemTextSize);
  fill(0);
  String mouseText = mouseX + "," + mouseY;
  text("POINTER " + mouseText, gridOffset, gridOffset * 4 - (gridOffset * 0.2));
}

////////////////////
/// GRID DRAWING ///
////////////////////
/*
* Draws the tool icons seen in the top left side of the screen. 
*/
private void drawToolGrid() {
  fill(palette.getPaletteColor(palette.black));
  textSize(systemTextSize);
  text("TOOLS", leftOffsetX, toolsOffsetY - 8);
  strokeWeight(4);
  for (int index = 0; index < icons.length; index++) {
    if (index == toolIndex) {
      stroke(palette.getPaletteColor(palette.lightGreen));
    } else {
      stroke(palette.getPaletteColor(palette.lightGrey));
    }
    rect(leftOffsetX*index + colorTileSize, toolsOffsetY, icons[index].width, icons[index].height);
    image(icons[index], leftOffsetX*index + colorTileSize, toolsOffsetY);
  }
}

/*
* Draws the colored squares seen in the mid-left of the screen, relative to the grid.
*/
private void drawColorPickerGrid() {
  noStroke();
  // Draw out the colour picker grid
  int colorIndex = 0;
  for (int y = 0; y < colorGrid.length; y++) {
    for (int x = 0; x < colorGrid[y].length; x++) {
      color fillColor = palette.getPaletteColor(colorIndex);
      fill(fillColor);
      square(x * colorTileSize + leftOffsetX, y * colorTileSize + colorGridOffsetY, colorTileSize);
      colorIndex++;
    }
  }
}

/*
* Draws out the grid that you can paint in. 
*/
private void drawPaintingGrid() {
  strokeWeight(2);
  // set the border color for all the tiles.
  stroke(palette.getPaletteColor(palette.lightGrey));
  // Draw the Grid
  for (int y = 0; y < grid.length; y++) {
    for (int x = 0; x < grid[y].length; x++) {
      // set the fill color as defined in our grid array before drawing the tile.
      fill(grid[y][x]);
      square(x * tileSize + gridOffset, y * tileSize + gridOffset, tileSize);
    }
  }
}

////////////////////////
/// HELPER FUNCTIONS ///
////////////////////////
/*
* Will check whether the mouse is where the Save Button is on screen or not.
* (boolean) returns: Will return true if the mouse is on the button, otherwise false.
*/
private boolean isPressingSave() {
  if (mouseX >= leftOffsetX && mouseX <= colorTileSize*2) {
    if (mouseY >= colorTileSize*2 && mouseY <= colorTileSize*3) {
      return true;
    }
  }
  return false;
}

/*
* Will check whether the mouse is where the "Save and Quit" Button is on screen or not.
* (boolean) returns: Will return true if the mouse is on the button, otherwise false.
*/
private boolean isPressingSaveAndQuit() {
  if (mouseX >= width-(colorTileSize*2) && mouseX <= width-colorTileSize) {
    if (mouseY >= colorTileSize*2 && mouseY <= colorTileSize*3) {
      return true;
    }
  }
  return false;
}

/*
* Will check whether the mouse is where the tools are on screen or not.
* (boolean) returns: Will return true if the mouse is on any of the tools buttons, otherwise false.
*/
private boolean isMouseInTools() {
  if (mouseX >= leftOffsetX && mouseX <= (leftOffsetX*2)*colorGrid.length) {
    if (mouseY >= toolsOffsetY && mouseY <= toolsOffsetY+colorTileSize) {
      return true;
    }
  }
  return false;
}

/*
* Will attempt to set the current used tool based on where on the tool grid the mouse is.
*/
private void setTool() {
  int result= constrain(floor((mouseX - leftOffsetX) / colorTileSize), 0, 3);
  if (result == lineToolIndex || result == circleToolIndex) {
    toolIndex = penToolIndex;
  } else {
    toolIndex = result;
  }
}

/*
* Will check whether the mouse is where the colors are on screen or not.
* (boolean) returns: Will return true if the mouse is on any of the color buttons, otherwise false.
*/
private boolean isMouseInColorPicker() {
  // we multiply the leftOffsetX value by 2 to get
  // the empty space between the screen and the first tile
  if (mouseX >= leftOffsetX && mouseX <= (leftOffsetX*2)*colorGrid.length) {
    if (mouseY >= colorGridOffsetY && mouseY <= colorGridOffsetY*colorGrid.length) {
      return true;
    }
  }
  return false;
}

/*
* Will attempt to set the current used colors based on where on the color grid the mouse is.
*/
private void setPickedColor() {
  color pixel = get(mouseX, mouseY);
  int paletteIndex = palette.getColorIndex(pixel);
  if (paletteIndex > -1) {
    color result = palette.getPaletteColor(paletteIndex);
    if (mouseButton == LEFT) {
      primaryPickedColor = result;
    } else {
      secondaryPickedColor = result;
    }
  }
}

/*
* Will check whether the mouse is where the grid is on screen or not.
* (boolean) returns: Will return true if the mouse is on any of the tiles in the grid, otherwise false.
*/
private boolean isMouseInGrid() {
  int gridStart = gridOffset; // upper left corner of grid.
  int gridEnd = gridStart + (grid[0].length * tileSize); // lower right corner of grid.
  if ((mouseX >= gridStart && mouseX <= gridEnd) && (mouseY >= gridStart && mouseY <= gridEnd)) {
    return true;
  }
  return false;
}

/*
* Will attempt to find coordinates corresponding to a location in the 2D grid array.
* (PVector) returns: Will return a vector with two indexes that exists in the 2D grid array. 
*/
private PVector getGridCoordinates() {
  int gridStart = gridOffset; // upper left corner of grid.
  int gridEnd = gridStart + (grid[0].length * tileSize); // lower right corner of grid.
  // Width and height of grid is the same
  int gridPxWidth = gridEnd - gridStart;
  // All cells are uniformly sized
  int cellPxWidth = gridPxWidth / grid.length;
  int xCell = constrain(floor((mouseX - gridOffset) / cellPxWidth), 0, grid.length - 1);
  int yCell = constrain(floor((mouseY - gridOffset) / cellPxWidth), 0, grid.length - 1);
  return new PVector(yCell, xCell);
}
