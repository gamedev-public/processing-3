/*
* This class is responsible for acting as a color palette. It keeps track of
* all the colors that can be used in this program and keeps an index of all 
* avaiable colors. This means that the color palette can always give you exactly
* the color you want, without you needing to know where in the array that the color
* is.
*/
class Palette {
  // All the colors are kept here.
  private color[] colors = new color[16];
  // Each of the colors are hard-coded below.
  int black = 0;
  int magenta = 1;
  int lightGreen = 2;
  int darkBlue = 3;
  int darkGrey = 4;
  int red = 5;
  int darkGreen = 6;
  int lightBlue = 7;
  int lightGrey = 8;
  int orange = 9;
  int teal = 10;
  int purple = 11;
  int white = 12;
  int yellow = 13;
  int darkTeal = 14;
  int pink = 15;
  
  // Here we construct the class itself, with all the colors that we want.
  Palette() {
    colors[0] = color(0, 0, 0);
    colors[1] = color(169, 49, 54);
    colors[2] = color(7, 169, 64);
    colors[3] = color(3, 109, 178);
    colors[4] = color(92, 99, 103);
    colors[5] = color(220, 47, 44);
    colors[6] = color(35, 153, 56);
    colors[7] = color(46, 156, 218);
    colors[8] = color(177, 179, 182);
    colors[9] = color(238, 183, 28);
    colors[10] = color(28, 164, 135);
    colors[11] = color(81, 48, 130);
    colors[12] = color(255, 255, 255);
    colors[13] = color(237, 212, 16);
    colors[14] = color(39, 92, 109);
    colors[15] = color(193, 45, 120);
  }
  
  /*
  * Use this method to retrieve a color from the palette.
  * (int) index: Use one of the predefined named colors above as index here.
  * (color) returns: The color found at that index. If the index is invalid, you will get an error.
  */
  color getPaletteColor(int index) {
    index = constrain(index, 0, colors.length - 1);
    return colors[index];
  }
  
  /*
  * Use this method to retrieve the index of a color.
  * (color) target: The color to try and find the index for.
  * (int) returns: The index of that color. Otherwise -1 if it doesn't exist.
  */
  int getColorIndex(color target) {
    for(int index = 0; index < colors.length; index++) {
      if(target == colors[index]) {
        return index;
      }
    }
    return -1;
  }
}
