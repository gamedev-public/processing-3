class Bird {
  private PImage bird_idle;
  private PImage bird_flap;
  private PVector center;
  private float gravity;
  private float force;
  private float velocity;
  private float velocityLimit;
  private boolean isGameRunning = true;
  
  Bird(PImage idle, PImage flap, float horizontalPosition, float verticalPosition, float drag, float jumpForce) {
    bird_idle = idle;
    bird_flap = flap;
    center = new PVector(horizontalPosition+(idle.width*0.5), verticalPosition + (idle.height*0.5));
    gravity = drag;
    velocityLimit = 1000;
    force = jumpForce;
  }
  
  void draw(double deltaTime) {
    if(isGameRunning == true) {
      velocity += gravity * deltaTime;
    }
    velocity = constrain(velocity, -velocityLimit, velocityLimit);
    center.y += velocity;
    if(velocity > 0) {
      image(bird_flap, center.x - (bird_idle.width * 0.5), center.y - (bird_idle.width * 0.5), bird_idle.width, bird_idle.height);
    } else {
      image(bird_idle, center.x - (bird_idle.width * 0.5), center.y - (bird_idle.width * 0.5), bird_idle.width, bird_idle.height);
    }
  }
  
  public void jump() {
    velocity = -force;
  }
  
  public PVector getCenter() {
    return center;
  }
  
  public boolean isTouchingCeiling() {
    float birdTop = center.y - (bird_idle.height * 0.5);
    if(birdTop < 0) {
      return true;
    }
    return false;
  }
  
  public boolean isTouchingGround() {
    float birdBottom = center.y + (bird_idle.height * 0.5);
    if(birdBottom > height) {
      return true;
    }
    return false;
  }
}
