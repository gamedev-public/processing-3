class Obstacle {
  private int gapSize;
  private int gapVerticalPosition;
  private float horizontalPosition;
  private PImage graphicTile;
  private PImage graphicTop;
  private float speed;
  private boolean isGameRunning = true;
  private boolean isPassed = false;

  Obstacle(int verticalGap, PImage obstacleTile, PImage obstacleTop, float screenMoveSpeed) {
    gapSize = verticalGap;
    graphicTile = obstacleTile;
    graphicTop = obstacleTop;
    speed = screenMoveSpeed;
    // derived fields
    int randomVerticalPosition = round(random(gapSize*2, height - (gapSize*2)));
    gapVerticalPosition = round((randomVerticalPosition / gapSize)) * gapSize;
    horizontalPosition = width + verticalGap;
  }

  /*
  * The Obstacles own draw function. Will interpolate the obstacle towards the left.
   */
  void draw(double deltaTime) {
    // if the game is running then we keep moving the obstacles
    // to the left. If not, then we'd like them to stop moving.
    if (isGameRunning == true) {
      horizontalPosition -= deltaTime * speed;
    }
    int derivedHeight = (gapVerticalPosition / (gapSize - 1)) * gapSize;
    scale(1, -1);
    for (int verticalPosition = 0; verticalPosition < (derivedHeight - gapSize); verticalPosition += gapSize) {
      if (verticalPosition < derivedHeight - gapSize*2) {
        image(graphicTile, horizontalPosition, -verticalPosition, gapSize, gapSize);
      } else {
        image(graphicTop, horizontalPosition, -verticalPosition, gapSize, gapSize);
      }
    }
    scale(1, -1);
    derivedHeight = gapVerticalPosition;// + gapSize;
    for (int verticalPosition = derivedHeight; verticalPosition < height; verticalPosition += gapSize) {
      if (verticalPosition == derivedHeight) {
        image(graphicTop, horizontalPosition, verticalPosition, gapSize, gapSize);
      } else {
        image(graphicTile, horizontalPosition, verticalPosition, gapSize, gapSize);
      }
    }
  }

  public boolean getIsGameRunning() {
    return isGameRunning;
  }

  /*
  * Used to set whether the game is running or not.
   */
  public void setIsGameRunning(boolean newState) {
    isGameRunning = newState;
  }

  public boolean getIsPassed() {
    return isPassed;
  }

  public void setIsPassed(boolean newState) {
    isPassed = newState;
  }

  /*
  * Used to determine whether the passed object is ahead of this obstacle or not.
   * (PVector) other: The center position of the object to compare to.
   * (integer) size: The uniform size (in pixels) of the object to compare to.
   * (boolean) returns: True if the object that's compared to is ahead of this obstacle, otherwise false.
   */
  public boolean isObstacleBehind(PVector other, int size) {
    float rightSide = horizontalPosition + gapSize;
    if ((other.x - (size * 0.5)) > rightSide) {
      return true;
    }
    return false;
  }

  /*
  * Used to determine whether the obstacle is offscreen or not.
   * (boolean) returns: True if the obstacle is offscreen, otherwise false.
   */
  public boolean isOffscreen() {
    if (horizontalPosition <= -gapSize) {
      return true;
    }
    return false;
  }

  /*
  * Used to determine whether the passed object is colliding with this obstacle or not.
   * (PVector) other: The center position of the object to compare to.
   * (integer) size: The uniform size (in pixels) of the object to compare to.
   * (boolean) returns: True if the object that's compared to is colliding with this obstacle, otherwise False.
   */
  public boolean isColliding(PVector other, int size) {
    float leftSide = horizontalPosition;
    float rightSide = horizontalPosition + gapSize;
    // the other object is not close to the left side or has passed already.
    if (other.x < leftSide || other.x > rightSide) {
      return false;
    } else {
      // the other object might be inside this obstacle.
      if (other.y < gapVerticalPosition - gapSize*2 || other.y > gapVerticalPosition) { //<>//
        return true;
      }
    }
    return false;
  }
}
