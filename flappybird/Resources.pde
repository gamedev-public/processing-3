/*
* Resource class holding constant references to all resources the game will need and helper functions.
 */
class Resources {
  PImage background;
  PImage birdIdle;
  PImage birdFlap;
  PImage birdPoop;
  PImage obstacleTile;
  PImage obstacleTop;
  PFont gameFont;
  String[] highscores;
  
  private String highscoreFilePath;
  private char highscoreSeparator = '¤';
  private PrintWriter writer;

  Resources() {
    // load in all the graphics
    background = loadImage("background.jpg");
    birdIdle = loadImage("bird_idle.png");
    birdFlap = loadImage("bird_flap.png");
    //birdPoop = loadImage("bird_poop.png");
    obstacleTile = loadImage("obstacle_tile.jpg");
    obstacleTop = loadImage("obstacle_top.jpg");
    gameFont = createFont("Consolas Bold", 16);
    // setup the highscore list
    highscoreFilePath = "highscores.txt";
    File highscoreFile = dataFile(highscoreFilePath);
    if (highscoreFile.isFile()) {
      highscores = loadStrings(highscoreFilePath);
    } else {
      createOutput(highscoreFilePath);
      highscores = new String[0];
    }
  }

  void writeNewHighscore(String name, int score) {
    String scoreString = score + highscoreSeparator + name;
    if (highscores.length == 0) {
      highscores = new String[1];
      highscores[0] = scoreString;
    } else if (highscores.length > 0 && highscores.length < 10) {
      highscores = (String[])append(scoreString, highscores);
      sort(highscores);
    } else if (highscores.length == 10) {
      for (int index = 0; index < highscores.length; index++) {
        String[] split = highscores[index].split(""+highscoreSeparator);
        int savedScore = parseInt(split[1]);
        if (savedScore < score) {
          highscores = (String[])splice(highscores, scoreString, index);
          break;
        }
      }
      highscores = shorten(highscores);
    }
    saveStrings(highscoreFilePath,new String[0]);
    writer = createWriter(highscoreFilePath);
    for(String characters : highscores) {
      writer.write(characters + "\n");
      writer.flush();
    }
    writer.close();
  }
}
