private Resources resources; //<>// //<>//

private double lastRecordedTime = 0;
private double deltaTime = 0;

private int maxObstaclesOnScreen = 4;
private float spawnDelay = 3;
private Obstacle[] obstacles;
private float obstacleSpeed  = 100.0;
private Bird bird;

private float elapsedTime = 0;
private int score = 0;
private boolean isGameRunning = true;

void setup() {
  size(712, 1024);
  obstacles = new Obstacle[0];
  resources = new Resources();
  createObstacle();
  bird = new Bird(resources.birdIdle, resources.birdFlap, resources.obstacleTile.width * 2, resources.obstacleTile.height * 4, 10, 4);
}

void draw() {
  deltaTime = ((millis() - lastRecordedTime) / 1000);
  background(resources.background);
  textFont(resources.gameFont);
  text("SCORE: " + score, 32, 32);
  if (elapsedTime < spawnDelay) {
    elapsedTime += deltaTime;
  } else {
    elapsedTime = 0;
    if (obstacles.length < maxObstaclesOnScreen) {
      createObstacle();
    }
  }
  if (obstacles.length > 0) {
    if(obstacles[0].isOffscreen() == true) {
      removeObstacle();
    }
    for (int index = 0; index < obstacles.length; index++) {
      obstacles[index].setIsGameRunning(isGameRunning);
      if (isGameRunning == true) {
        if (obstacles[index].isColliding(bird.getCenter(), resources.birdIdle.width)) {
          // end game
          println("Bird touched an Obstacle");
          isGameRunning = false;
          break;
        }
        if (obstacles[index].isObstacleBehind(bird.getCenter(), resources.birdIdle.width)) {
            if (obstacles[index].getIsPassed() == false) {
              obstacles[index].setIsPassed(true);
              score += 1;
            }
          }
      }
      obstacles[index].draw(deltaTime);
    }
    bird.draw(deltaTime);
    if(bird.isTouchingCeiling() == true) {
      println("Bird touched the Ceiling");
      isGameRunning = false;
    }
    if(bird.isTouchingGround() == true) {
      println("Bird touched the Ground");
      isGameRunning = false;
    }
  }
  lastRecordedTime = millis();
}

void keyPressed() {
  if(isGameRunning == true) {
    if(key == ' ') {
      bird.jump();
    }
  }
}

void createObstacle() {
  obstacles = (Obstacle[])append(obstacles, new Obstacle(resources.obstacleTile.width, resources.obstacleTile, resources.obstacleTop, obstacleSpeed));
}

void removeObstacle() {
  obstacles[0] = null;
  obstacles = (Obstacle[])subset(obstacles, 1, obstacles.length-1);
}
